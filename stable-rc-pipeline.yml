stages:
  - trigger
  - build
  - test

variables:
  DOCKER_DRIVER: overlay2
  DOCKER_IMAGE_TAG: v4
  GCC_VER: "11.1.0"
  GIT_STRATEGY: none
  RETRY: 3
  CHECK_KERNEL_REPOSITORY: https://git.kernel.org/pub/scm/linux/kernel/git/stable/linux-stable-rc.git
  BUILD_BRANCH: ${CI_COMMIT_REF_NAME}

.trigger:
  stage: trigger
  image: registry.gitlab.com/cip-project/cip-testing/linux-cip-ci:build-$DOCKER_IMAGE_TAG
  tags:
    - small
  script:
    - 'echo "Branch: ${STABLE_RC_BRANCH}"'
    - git config --global user.email "${CIP_GIT_EMAIL}"
    - git config --global user.name "${CIP_GIT_NAME}"
    - |-
      n=0
      echo "Cloning linux-stable-rc-ci"
      until [ $n -ge $RETRY ]
      do
        git clone https://${CIP_GIT_USER}:${CIP_GIT_TOKEN}@gitlab.com/cip-project/cip-testing/linux-stable-rc-ci.git && break
        n=$((n+1))
        sleep 10
      done
      if [ $n -ge $RETRY ]
      then
        exit 1
      fi
    - pushd linux-stable-rc-ci
    - git checkout ${STABLE_RC_BRANCH}
    - git log --pretty=oneline --graph -10
    - 'echo "Checking branch ${STABLE_RC_BRANCH} for updates"'
    - git remote add upstream ${CHECK_KERNEL_REPOSITORY} || true
    - git fetch --depth=1 upstream ${STABLE_RC_BRANCH}
    - git log upstream/${STABLE_RC_BRANCH} -1 | tee linux-stable-rc.version
    - git add linux-stable-rc.version
    - |-
      git commit --author="$(git show -s --format='%an <%ae>' upstream/${STABLE_RC_BRANCH} -1)" \
      -am "$(git log --pretty=oneline upstream/${STABLE_RC_BRANCH} -1 | cut -d' ' -f2-) \
      ($(git log --pretty=oneline upstream/${STABLE_RC_BRANCH} -1 | cut -c1-12))" \
      -m "$(git log upstream/${STABLE_RC_BRANCH} -1)" && \
      git push https://${CIP_GIT_USER}:${CIP_GIT_TOKEN}@gitlab.com/cip-project/cip-testing/linux-stable-rc-ci.git ${STABLE_RC_BRANCH} || true
    - git log --pretty=oneline -10

.build-base:
  stage: build
  tags:
    - large
  script:
    - |-
      echo "Cloning linux repository"
      n=0
      until [ $n -ge $RETRY ]
      do
        git clone --depth 10 --branch ${BUILD_BRANCH} ${CHECK_KERNEL_REPOSITORY} . && break
        n=$((n+1))
        sleep 10
      done
      if [ $n -ge $RETRY ]
      then
        exit 1
      fi
    - echo "Using ${BUILD_BRANCH}"
    - git log --pretty=oneline --graph
    - /opt/build_kernel.sh && /opt/build_report.sh "pass" || /opt/build_report.sh "fail"
    - 'echo "We have just built: $(git log --pretty=oneline -1)"'
  artifacts:
    name: "$CI_JOB_NAME"
    when: always
    expire_in: 1 month
    paths:
      - output

.build:
  extends: .build-base
  image: registry.gitlab.com/cip-project/cip-testing/linux-cip-ci:build-$DOCKER_IMAGE_TAG

.build-debian-base:
  extends: .build-base
  image: registry.gitlab.com/cip-project/cip-testing/linux-cip-ci:build-$DEB_SUITE-$DOCKER_IMAGE_TAG

.build-debian-buster:
  extends: .build-debian-base
  variables:
    DEB_SUITE: buster

.build-debian-bullseye:
  extends: .build-debian-base
  variables:
    DEB_SUITE: bullseye

.build-debian-bookworm:
  extends: .build-debian-base
  variables:
    DEB_SUITE: bookworm

.test:
  stage: test
  image: registry.gitlab.com/cip-project/cip-testing/linux-cip-ci:test-$DOCKER_IMAGE_TAG
  tags:
    - small
  script:
    - /opt/submit_tests.sh
  artifacts:
    name: "$CI_JOB_NAME"
    when: always
    expire_in: 1 month
    paths:
      - output
      - results
    reports:
      junit: results/results*.xml
